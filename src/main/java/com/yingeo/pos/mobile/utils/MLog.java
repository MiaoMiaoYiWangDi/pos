package com.yingeo.pos.mobile.utils;

import android.text.TextUtils;

public class MLog {

    static boolean enablePrint = false;

    public static void d(String message) {
        d(null, message);
    }

    public static void d(String tag, String message) {
        if (!enablePrint) return;
        if (!TextUtils.isEmpty(tag)) {
            System.out.println(tag + " ::: " + message);
        } else {
            System.out.println(message);
        }
    }
}
