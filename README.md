<p align="center">
	<a href="https://www.yingeo.com"><img src="https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_4905888856a13754ebaafa3ff42704fe_r.png" width="30%"></a>
</p>
<p align="center">
	<strong>银歌收银系统 - 新一代开店软件，智慧零售，数字门店解决方案</strong>
</p>
<p align="center">
	👉 <a href="https://www.yingeo.com">https://www.yingeo.com</a> 👈
</p>

-------------------------------------------------------------------------------

## 📚 项目介绍

银歌收银系统（YINGEO-POS）是一套全平台综合型收银系统，领先SaaS云端技术收银系统，支持连锁，单店，联营等多种模式，线上线下联动，高度融合，到店、到家多生意形态。

银歌收银系统使用`Spring Cloud`和`Ant Design Vue`开发，集成`Spring Security`实现权限管理功能，是一套非常实用的web开发框架。

### 🎁 名称的由来

YINGEO = 银歌，是由原红河华云信息技术有限公司旗下分公司（深圳市银歌云技术有限公司）带领团队深度研发，凭借丰富的行业经验和敏锐的商业洞察，利用云计算、移动互联网、人工智能和物联网等先进技术，为企业提供POS-ERP软件、移动支付、全渠道营销及大数据运营等数字化服务，通过构建具有前瞻性的智慧商业模式，帮助企业实现商业价值的全面提升。


### 🍟 项目体验

- 银歌收银系统后台体验：[https://pos.yingeo.com/](https://pos.yingeo.com "银歌收银系统后台体验")
- 银歌收银系统android收银端下载体验：[https://www.yingeo.com/1.apk](https://www.yingeo.com/1.apk "android收银端下载")
- 银歌收银系统macos收银端下载体验：[https://www.yingeo.com/yingeo.dmg](https://www.yingeo.com/yingeo.dmg "macos收银端下载")
- 银歌收银系统windows收银端下载体验：[https://www.yingeo.com/yingeo.exe](https://www.yingeo.com/yingeo.exe "windows收银端下载")
- 银歌收银移动版，苹果IOS，App Store 搜索（银歌收银）下载即可
- 银歌收银移动版，安卓端：[https://www.yingeo.com/2.apk](https://www.yingeo.com/2.apk "银歌收银移动版下载")

### 🍎 项目特点

* 微服务架构，支持分布式部署，高并发
* 提供http形式接口，提供各语言的`sdk`实现，方便对接
* 接口请求和响应数据采用签名机制，保证交易安全可靠
* 管理平台操作界面简洁、易用
* 支付平台到商户系统的订单通知使用MQ实现，保证了高可用，消息可达
* 使用`spring security`实现权限管理
* 前后端分离架构，方便二次开发
* 由原红河华云信息技术`银歌`团队开发，有着多年大型系统研发经验

***
微信扫描下方二维码，关注官方公众号：银歌收银，获取更多精彩内容。

![银歌收银公众号](https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_2f588325ef5e83fdea9abfecf8b82139_r.jpg "银歌收银公众号")

## 🥞 系统架构

> 银歌收银系统系统架构图

![银歌系统架构图](https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_e245f6763992f446921988fb4548f4a6_r.jpg "银歌系统架构图")

> 核心技术栈

| 软件名称  | 描述 | 版本|
|---|---|---|
|Jdk | Java环境 | 1.8|
|Spring Cloud | 开发框架 | 2.4.5|
|Redis | 分布式缓存 | 3.2.8 或 高版本|
|MySQL | 数据库 | 5.7.X 或 8.0 高版本|
|MQ | 消息中间件 | ActiveMQ 或 RabbitMQ 或 RocketMQ|
|[Ant Design Vue](https://www.antdv.com/docs/vue/introduce-cn/) | Ant Design的Vue实现，前端开发使用 | 2.1.2|
|[MyBatis-Plus](https://mp.baomidou.com/) | MyBatis增强工具 | 3.4.2|
|[WxJava](https://gitee.com/binary/weixin-java-tools) | 微信开发Java SDK | 4.1.0|
|[Hutool](https://www.hutool.cn/) | Java工具类库 | 5.6.6|

> 项目结构

```lua
YINGEO-POS
├── eureka-server -- 注册中心
├── conf-server -- 配置中心
├── zuul-server -- 网关服务
├── platform-server -- 平台服务
├── es-server -- 店铺服务
├── commodity-server -- 商品服务
├── order-server -- 订单服务
├── pay-server -- 支付服务
├── push-server -- 推送服务
├── wechat-server -- 微信服务
└── alipay-server -- 支付宝服务
```

## 🍯 系统截图

`以下截图是从实际已完成功能界面截取,截图时间为：2021-12-12 12:12`

![银歌收银端演示界面](https://www.yingeo.com/img/syd1.png "银歌收银端演示界面")

![银歌收银端演示界面](https://www.yingeo.com/img/syd2.png "银歌收银端演示界面")

![银歌后台演示界面](https://www.yingeo.com/img/ybp.png "银歌后台演示界面")

![银歌后台演示界面](https://www.yingeo.com/img/sj.png "银歌后台演示界面")

![银歌后台演示界面](https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_f38e5b3f0817b18a833b62a61fba93db_r.png)

![银歌后台演示界面](https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_c56a08371b8aa77de52710633465e1b0_r.png)

![银歌移动收银演示界面](https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_19ceba3742f2f66fb19adec4d9969e27_r.jpg "银歌移动收银演示界面")

![银歌掌柜演示界面](https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_52845447f625e5e1bc95f2d963d593f1_r.jpg "银歌掌柜演示界面")

![银歌会员演示界面](https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_77b6889c926b166a30413e40c8dbcd7e_r.jpg "银歌会员演示界面")

![银歌收银小程序商城演示界面](https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_b8cbf7654651a8b47bee9d1ee25626b1_r.jpg "银歌收银小程序商城演示界面")

#### 平台数据可视化截图
![银歌平台](https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_800790cad3f16a6e70aea9516fea9648_r.png "银歌平台")

![银歌平台](https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_d796a64c63fd74be5d9886a3f5dd5a4c_r.png "银歌平台")

![银歌平台](https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_bff56e402b16b42e1e42cd15a22f7ea0_r.png "银歌平台")

## 🥪 关于我们
***
微信扫描下方二维码，关注官方公众号：银歌收银，获取更多精彩内容。

![银歌收银公众号](https://doc.yingeo.com/wiki/uploads/yingeopos/images/m_84fa9e20c00fe6ca4564572e545cc6e3_r.png "银歌收银公众号")